package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.impl.campaign.ids.HullMods;
import java.util.HashMap;
import java.util.Map;

public class ConvertedHangar extends BaseHullMod {

    public static final int CREW_REQ = 20;

    private static final Map<HullSize, Float> mag = new HashMap<>(4);

    static {
        mag.put(HullSize.FRIGATE, 0f);
        mag.put(HullSize.DESTROYER, 75f);
        mag.put(HullSize.CRUISER, 50f);
        mag.put(HullSize.CAPITAL_SHIP, 25f);
    }

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getFighterRefitTimeMult().modifyPercent(id, mag.get(hullSize));
        stats.getNumFighterBays().modifyFlat(id, 1f);

        stats.getMinCrewMod().modifyFlat(id, CREW_REQ);
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + mag.get(HullSize.DESTROYER).intValue() + "%";
        }
        if (index == 1) {
            return "" + mag.get(HullSize.CRUISER).intValue() + "%";
        }
        if (index == 2) {
            return "" + mag.get(HullSize.CAPITAL_SHIP).intValue() + "%";
        }
        if (index == 3) {
            return "" + CREW_REQ;
        }
        return null;
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (ship != null && ship.isFrigate()) {
            return "Can not be installed on a frigate";
        }
        if (ship != null && ship.getHullSpec().getFighterBays() > 0) {
            return "Ship has standard fighter bays";
        }
        if (ship != null && ship.getVariant().hasHullMod("diableavionics_cramped")) {
            return "Cannot be installed on a cramped hull";
        }
        if (ship != null && ship.getVariant().hasHullMod("swp_arcade")) {
            return "Can not be installed on the Helios";
        }
        return "Can not be installed on a phase ship";
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return ship != null && !ship.isFrigate() && ship.getHullSpec().getFighterBays() <= 0
                && !ship.getVariant().hasHullMod(HullMods.PHASE_FIELD)
                && !ship.getVariant().hasHullMod("swp_arcade")
                && !ship.getVariant().getHullMods().contains("diableavionics_cramped");
    }
}
