package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import java.util.HashMap;
import java.util.Map;

public class HeavyArmor extends BaseHullMod {

    public static final float MANEUVER_PENALTY = 20f;

    private final static Map<HullSize, Float> mag = new HashMap<>(4);

    static {
        mag.put(HullSize.FRIGATE, 100f);
        mag.put(HullSize.DESTROYER, 200f);
        mag.put(HullSize.CRUISER, 300f);
        mag.put(HullSize.CAPITAL_SHIP, 400f);
    }

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getArmorBonus().modifyFlat(id, mag.get(hullSize));

        stats.getAcceleration().modifyMult(id, 1f - MANEUVER_PENALTY * 0.01f);
        stats.getDeceleration().modifyMult(id, 1f - MANEUVER_PENALTY * 0.01f);
        stats.getTurnAcceleration().modifyMult(id, 1f - MANEUVER_PENALTY * 0.01f);
        stats.getMaxTurnRate().modifyMult(id, 1f - MANEUVER_PENALTY * 0.01f);
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + (mag.get(HullSize.FRIGATE)).intValue();
        }
        if (index == 1) {
            return "" + (mag.get(HullSize.DESTROYER)).intValue();
        }
        if (index == 2) {
            return "" + (mag.get(HullSize.CRUISER)).intValue();
        }
        if (index == 3) {
            return "" + (mag.get(HullSize.CAPITAL_SHIP)).intValue();
        }
        if (index == 4) {
            return "" + (int) MANEUVER_PENALTY;
        }
        return null;
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (ship != null && ship.getVariant().getHullMods().contains("brimaginosregen")) {
            return "Incompatible with Nanolattice Armor";
        }
        if (ship != null && ship.getVariant().getHullMods().contains("SCY_lightArmor")) {
            return "Incompatible with Lightweight Plating";
        }
        if (ship != null && ship.getVariant().getHullMods().contains("ii_elite_package")) {
            return "Incompatible with Imperial Elite Package";
        }
        return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return ship != null && !ship.getVariant().getHullMods().contains("brimaginosregen")
                && !ship.getVariant().getHullMods().contains("SCY_lightArmor")
                && !ship.getVariant().getHullMods().contains("ii_elite_package");
    }
}
