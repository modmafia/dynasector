package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;

public class FluxBreakers extends BaseHullMod {

    public static final float FLUX_RESISTANCE = 50f;
    public static final float VENT_RATE_BONUS = 25f;

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getEmpDamageTakenMult().modifyMult(id, 1f - FLUX_RESISTANCE * 0.01f);
        stats.getVentRateMult().modifyPercent(id, VENT_RATE_BONUS);
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + (int) FLUX_RESISTANCE;
        }
        if (index == 1) {
            return "" + (int) VENT_RATE_BONUS;
        }
        return null;
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (ship != null && ship.getVariant().getHullMods().contains("ii_elite_package")) {
            return "Incompatible with Imperial Elite Package";
        }
        return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return ship != null && !ship.getVariant().getHullMods().contains("ii_elite_package");
    }
}
