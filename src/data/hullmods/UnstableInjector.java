package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import java.util.HashMap;
import java.util.Map;

public class UnstableInjector extends BaseHullMod {

    private static final float RANGE_MULT = 0.85f;

    private static final Map<HullSize, Float> mag = new HashMap<>(4);

    static {
        mag.put(HullSize.FRIGATE, 25f);
        mag.put(HullSize.DESTROYER, 20f);
        mag.put(HullSize.CRUISER, 15f);
        mag.put(HullSize.CAPITAL_SHIP, 15f);
    }

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getMaxSpeed().modifyFlat(id, mag.get(hullSize));
        stats.getBallisticWeaponRangeBonus().modifyMult(id, RANGE_MULT);
        stats.getEnergyWeaponRangeBonus().modifyMult(id, RANGE_MULT);
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + mag.get(HullSize.FRIGATE).intValue();
        }
        if (index == 1) {
            return "" + mag.get(HullSize.DESTROYER).intValue();
        }
        if (index == 2) {
            return "" + mag.get(HullSize.CRUISER).intValue();
        }
        if (index == 3) {
            return "" + mag.get(HullSize.CAPITAL_SHIP).intValue();
        }
        if (index == 4) {
            return "" + Math.round((1f - RANGE_MULT) * 100f);
        }
        return null;
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (ship != null && ship.getVariant().getHullMods().contains("brdrive")) {
            return "Incompatible with BRDY Drive Conversion";
        }
        if (ship != null && ship.getVariant().getHullMods().contains("SCY_engineering")) {
            return "Incompatible with Scyan engines";
        }
        if (ship != null && ship.getVariant().getHullMods().contains("ii_targeting_package")) {
            return "Incompatible with Imperial Targeting Package";
        }

        return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return !ship.getVariant().getHullMods().contains("brdrive")
                && !ship.getVariant().getHullMods().contains("SCY_engineering")
                && !ship.getVariant().getHullMods().contains("ii_targeting_package");
    }
}
