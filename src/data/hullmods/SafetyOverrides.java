package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.impl.campaign.ids.HullMods;
import com.fs.starfarer.api.util.Misc;
import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

public class SafetyOverrides extends BaseHullMod {

    private static final float FLUX_DISSIPATION_MULT = 2f;

    private static final float PEAK_MULT = 0.33f;
    private static final float RANGE_MULT = 0.25f;
    private static final float RANGE_THRESHOLD = 450f;
    private static final Color color = new Color(255, 100, 255, 255);
    private static final Map<HullSize, Float> speed = new HashMap<>(4);

    static {
        speed.put(HullSize.FRIGATE, 50f);
        speed.put(HullSize.DESTROYER, 30f);
        speed.put(HullSize.CRUISER, 20f);
        speed.put(HullSize.CAPITAL_SHIP, 10f);
    }

    @Override
    public void advanceInCombat(ShipAPI ship, float amount) {
        ship.getEngineController().fadeToOtherColor(this, color, null, 1f, 0.4f);
        ship.getEngineController().extendFlame(this, 0.25f, 0.25f, 0.25f);
    }

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getMaxSpeed().modifyFlat(id, speed.get(hullSize));
        stats.getAcceleration().modifyFlat(id, speed.get(hullSize) * 2f);
        stats.getDeceleration().modifyFlat(id, speed.get(hullSize) * 2f);
        stats.getZeroFluxMinimumFluxLevel().modifyFlat(id, 2f); // set to two, meaning boost is always on

        stats.getFluxDissipation().modifyMult(id, FLUX_DISSIPATION_MULT);

        stats.getPeakCRDuration().modifyMult(id, PEAK_MULT);

        stats.getVentRateMult().modifyMult(id, 0f);

        stats.getWeaponRangeThreshold().modifyFlat(id, RANGE_THRESHOLD);
        stats.getWeaponRangeMultPastThreshold().modifyMult(id, RANGE_MULT);
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + speed.get(HullSize.FRIGATE).intValue();
        }
        if (index == 1) {
            return "" + speed.get(HullSize.DESTROYER).intValue();
        }
        if (index == 2) {
            return "" + speed.get(HullSize.CRUISER).intValue();
        }
        if (index == 3) {
            return Misc.getRoundedValue(FLUX_DISSIPATION_MULT);
        }
        if (index == 4) {
            return Misc.getRoundedValue(PEAK_MULT);
        }
        if (index == 5) {
            return Misc.getRoundedValue(RANGE_THRESHOLD);
        }
        return null;
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (ship.getVariant().getHullSize() == HullSize.CAPITAL_SHIP) {
            return "Can not be installed on capital ships";
        }
        if (ship.getVariant().hasHullMod(HullMods.CIVGRADE)) {
            return "Can not be installed on civilian ships";
        }
        if (ship.getVariant().hasHullMod("swp_arcade")) {
            return "Can not be installed on the Helios";
        }
        if (ship.getVariant().getHullMods().contains("brdrive")) {
            return "Incompatible with BRDY Drive Conversion";
        }
        if (ship.getVariant().getHullMods().contains("ii_targeting_package")) {
            return "Incompatible with Imperial Targeting Package";
        }

        return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        if (ship.getVariant().getHullSize() == HullSize.CAPITAL_SHIP) {
            return false;
        } else if (ship.getVariant().hasHullMod(HullMods.CIVGRADE)) {
            return false;
        } else if (ship.getVariant().hasHullMod("swp_arcade")) {
            return false;
        } else if (ship.getVariant().hasHullMod("brdrive")) {
            return false;
        } else if (ship.getVariant().hasHullMod("ii_targeting_package")) {
            return false;
        }

        return true;
    }
}
