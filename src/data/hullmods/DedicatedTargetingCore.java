package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import java.util.HashMap;
import java.util.Map;

public class DedicatedTargetingCore extends BaseHullMod {

    private static final Map<HullSize, Float> mag = new HashMap<>(5);

    static {
        mag.put(HullSize.FIGHTER, 0f);
        mag.put(HullSize.FRIGATE, 0f);
        mag.put(HullSize.DESTROYER, 0f);
        mag.put(HullSize.CRUISER, 35f);
        mag.put(HullSize.CAPITAL_SHIP, 50f);
    }

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getBallisticWeaponRangeBonus().modifyPercent(id, mag.get(hullSize));
        stats.getEnergyWeaponRangeBonus().modifyPercent(id, mag.get(hullSize));
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + mag.get(HullSize.CRUISER).intValue();
        }
        if (index == 1) {
            return "" + mag.get(HullSize.CAPITAL_SHIP).intValue();
        }
        return null;
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (ship != null && ship.getHullSize() != HullSize.CAPITAL_SHIP && ship.getHullSize() != HullSize.CRUISER) {
            return "Can only be installed on cruisers and capital ships";
        }
        if (ship != null && ship.getVariant().getHullMods().contains("targetingunit")) {
            return "Incompatible with Integrated Targeting Unit";
        }
        if (ship != null && ship.getVariant().getHullMods().contains("advancedcore")) {
            return "Incompatible with Advanced Targeting Core";
        }
        if (ship != null && ship.getVariant().getHullMods().contains("brtarget")) {
            return "Incompatible with BRDY Strike Suite";
        }
        if (ship != null && ship.getVariant().getHullMods().contains("supercomputer")) {
            return "Incompatible with Targeting Supercomputer";
        }
        if (ship != null && ship.getVariant().getHullMods().contains("ii_armor_package")) {
            return "Incompatible with Imperial Armor Package";
        }
        return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return (ship.getHullSize() == HullSize.CAPITAL_SHIP || ship.getHullSize() == HullSize.CRUISER)
                && !ship.getVariant().getHullMods().contains("targetingunit")
                && !ship.getVariant().getHullMods().contains("advancedcore")
                && !ship.getVariant().getHullMods().contains("brtarget")
                && !ship.getVariant().getHullMods().contains("supercomputer")
                && !ship.getVariant().getHullMods().contains("ii_armor_package");
    }
}
