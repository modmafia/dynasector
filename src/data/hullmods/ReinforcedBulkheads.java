package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.impl.campaign.ids.Stats;

public class ReinforcedBulkheads extends BaseHullMod {

    public static final float HULL_BONUS = 40f;

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getHullBonus().modifyPercent(id, HULL_BONUS);

        stats.getDynamic().getMod(Stats.INDIVIDUAL_SHIP_RECOVERY_MOD).modifyFlat(id, 1000f);
        stats.getBreakProb().modifyMult(id, 0f);
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + (int) HULL_BONUS;
        }
        return null;
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (ship.getVariant().hasHullMod("tem_excaliburdrive")) {
            return "Incompatible with Excalibur Drive";
        }
        if (ship.getVariant().getHullMods().contains("ii_elite_package")) {
            return "Incompatible with Imperial Elite Package";
        }

        return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        if (ship.getVariant().hasHullMod("tem_excaliburdrive")) {
            return false;
        }
        if (ship.getVariant().hasHullMod("ii_elite_package")) {
            return false;
        }

        return true;
    }
}
