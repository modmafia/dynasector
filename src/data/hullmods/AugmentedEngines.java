package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import java.util.HashMap;
import java.util.Map;

public class AugmentedEngines extends BaseHullMod {

    public static final float REPAIR_PENALTY = 25f;
    private static final int BURN_LEVEL_BONUS = 1;
    private static final int PROFILE_PENALTY = 50;
    private static final int STRENGTH_PENALTY = 50;

    private static final Map<HullSize, Float> mag = new HashMap<>(4);

    static {
        mag.put(HullSize.FRIGATE, 50f);
        mag.put(HullSize.DESTROYER, 35f);
        mag.put(HullSize.CRUISER, 30f);
        mag.put(HullSize.CAPITAL_SHIP, 25f);
    }

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getSensorProfile().modifyPercent(id, PROFILE_PENALTY);
        stats.getSensorStrength().modifyMult(id, 1f - STRENGTH_PENALTY * 0.01f);

        stats.getMaxBurnLevel().modifyFlat(id, BURN_LEVEL_BONUS);
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + BURN_LEVEL_BONUS;
        }
        if (index == 1) {
            return "" + STRENGTH_PENALTY;
        }
        if (index == 2) {
            return "" + PROFILE_PENALTY;
        }
        return null;
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (ship != null && ship.getVariant().getHullMods().contains("brdrive")) {
            return "Incompatible with BRDY Drive Conversion";
        }
        if (ship != null && ship.getVariant().getHullMods().contains("SCY_engineering")) {
            return "Incompatible with Scyan engines";
        }
        if (ship != null && ship.getVariant().getHullMods().contains("tow_cable")) {
            return "Incompatible with Monofilament Tow Cable";
        }
        if (ship != null && ship.getVariant().getHullMods().contains("ii_targeting_package")) {
            return "Incompatible with Imperial Targeting Package";
        }

        return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return ship != null && !ship.getVariant().getHullMods().contains("brdrive")
                && !ship.getVariant().getHullMods().contains("SCY_engineering")
                && !ship.getVariant().getHullMods().contains("tow_cable")
                && !ship.getVariant().getHullMods().contains("ii_targeting_package");
    }
}
