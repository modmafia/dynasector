package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;

public class ArmoredWeapons extends BaseHullMod {

    public static final float ARMOR_BONUS = 10f;
    public static final float HEALTH_BONUS = 100f;
    public static final float TURN_PENALTY = 25f;

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getArmorBonus().modifyPercent(id, ARMOR_BONUS);
        stats.getWeaponHealthBonus().modifyPercent(id, HEALTH_BONUS);
        stats.getWeaponTurnRateBonus().modifyMult(id, 1f - TURN_PENALTY * 0.01f);
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + (int) HEALTH_BONUS;
        }
        if (index == 1) {
            return "" + (int) TURN_PENALTY;
        }
        if (index == 2) {
            return "" + (int) ARMOR_BONUS;
        }
        return null;
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (ship != null && ship.getVariant().getHullMods().contains("brimaginosregen")) {
            return "Incompatible with Nanolattice Armor";
        }
        if (ship != null && ship.getVariant().getHullMods().contains("ii_elite_package")) {
            return "Incompatible with Imperial Elite Package";
        }

        return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return ship != null && !ship.getVariant().getHullMods().contains("brimaginosregen")
                && !ship.getVariant().getHullMods().contains("ii_elite_package");
    }
}
