package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShieldAPI;
import com.fs.starfarer.api.combat.ShieldAPI.ShieldType;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;

public class FrontShieldGenerator extends BaseHullMod {

    public static final float SHIELD_ARC = 90f;
    public static final float SPEED_MULT = 0.8f;

    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id) {
        ShieldAPI shield = ship.getShield();
        if (shield == null) {
            ship.setShield(ShieldType.FRONT, 0.5f, 1.2f, SHIELD_ARC);
        }
    }

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getMaxSpeed().modifyMult(id, SPEED_MULT);
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + (int) SHIELD_ARC;
        }
        if (index == 1) {
            return "" + Math.round((1f - SPEED_MULT) * 100f) + "%";
        }
        return null;
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (ship != null && (ship.getHullSpec().getDefenseType() == ShieldType.FRONT ||
                             ship.getHullSpec().getDefenseType() == ShieldType.OMNI)) {
            return "Ship already has shields";
        }
        if (ship != null && ship.getHullSpec().getDefenseType() != ShieldType.NONE && ship.getPhaseCloak() != null) {
            return "Incompatible with " + ship.getPhaseCloak().getDisplayName();
        }
        if (ship != null && ship.getVariant().getHullMods().contains("ExigencyBeamArmor")) {
            return "Incompatible with Electrostatic Armor";
        }
        if (ship != null && ship.getVariant().getHullMods().contains("tem_latticeshield")) {
            return "Incompatible with Lattice Shield Matrix";
        }
        if (ship != null && ship.getVariant().getHullMods().contains("swp_shieldbypass")) {
            return "Incompatible with Shield Bypass";
        }

        return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return ship != null && ship.getHullSpec().getDefenseType() == ShieldType.NONE &&
                !ship.getVariant().getHullMods().contains("ExigencyRegen") &&
                !ship.getVariant().getHullMods().contains("tem_latticeshield") &&
                !ship.getVariant().getHullMods().contains("swp_shieldbypass");
    }
}
