package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;

public class StabilizedShieldEmitter extends BaseHullMod {

    public static final float SHIELD_UPKEEP_BONUS = 50f;

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getShieldUpkeepMult().modifyMult(id, 1f - SHIELD_UPKEEP_BONUS * 0.01f);
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + (int) SHIELD_UPKEEP_BONUS;
        }
        return null;
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        return "Ship has no shields";
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return ship != null && ship.getShield() != null &&
                !ship.getVariant().getHullMods().contains("swp_shieldbypass");
    }
}
