package data.scripts;

import com.thoughtworks.xstream.XStream;
import data.scripts.campaign.submarkets.DS_ExipiratedAvestaSubmarketPlugin;
import data.scripts.campaign.submarkets.DS_II_EBaySubmarketPlugin;
import data.scripts.campaign.submarkets.DS_II_MilitaryIndustrialSubmarketPlugin;
import data.scripts.campaign.submarkets.DS_TEM_MarketPlugin;
import data.scripts.campaign.submarkets.DS_UW_CabalMarketPlugin;
import data.scripts.campaign.submarkets.DS_UW_ScrapyardMarketPlugin;

public class DS_ModPluginAlt {

    static void configureModdedXStream(XStream x) {
        if (DSModPlugin.templarsExists) {
            x.alias("DS_TEM_MarketPlugin", DS_TEM_MarketPlugin.class);
        }
        if (DSModPlugin.exigencyExists) {
            x.alias("DS_ExipiratedAvestaSubmarketPlugin", DS_ExipiratedAvestaSubmarketPlugin.class);
        }
        if (DSModPlugin.imperiumExists) {
            x.alias("DS_II_EBaySubmarketPlugin", DS_II_EBaySubmarketPlugin.class);
            x.alias("DS_II_MilitaryIndustrialSubmarketPlugin", DS_II_MilitaryIndustrialSubmarketPlugin.class);
        }
        if (DSModPlugin.hasUnderworld) {
            x.alias("DS_UW_CabalMarketPlugin", DS_UW_CabalMarketPlugin.class);
            x.alias("DS_UW_ScrapyardMarketPlugin", DS_UW_ScrapyardMarketPlugin.class);
        }
    }
}
