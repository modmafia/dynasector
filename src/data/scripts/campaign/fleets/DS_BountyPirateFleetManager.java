package data.scripts.campaign.fleets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FleetAssignment;
import com.fs.starfarer.api.campaign.LocationAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.impl.campaign.fleets.BountyPirateFleetManager;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactory.MercType;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.MemFlags;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.campaign.DS_FleetFactory;
import data.scripts.campaign.DS_FleetFactory.FleetFactoryDelegate;
import org.lwjgl.util.vector.Vector2f;

public class DS_BountyPirateFleetManager extends BountyPirateFleetManager {

    @Override
    protected CampaignFleetAPI spawnFleet() {
        final StarSystemAPI system = currPlayerLoc;
        if (system == null) {
            return null;
        }

        CampaignFleetAPI player = Global.getSector().getPlayerFleet();
        if (player == null) {
            return null;
        }
        float distToPlayerLY = Misc.getDistanceLY(player.getLocationInHyperspace(), system.getLocation());
        if (distToPlayerLY > 1f) {
            return null;
        }

        WeightedRandomPicker<MercType> picker = new WeightedRandomPicker<>();
        picker.add(MercType.SCOUT, 10f);
        picker.add(MercType.BOUNTY_HUNTER, 10f);
        picker.add(MercType.PRIVATEER, 10f);
        picker.add(MercType.PATROL, 10f);
        picker.add(MercType.ARMADA, 3f);

        MercType type = picker.pick();

        final float combat;
        final float tanker;
        final float freighter;
        final String fleetType = type.fleetType;
        switch (type) {
            default:
            case SCOUT:
                combat = Math.round(1f + (float) Math.random() * 2f);
                freighter = 0f;
                tanker = 0f;
                break;
            case PRIVATEER:
            case BOUNTY_HUNTER:
                combat = Math.round(3f + (float) Math.random() * 2f);
                freighter = Math.round(0f + (float) Math.random() * 2f);
                tanker = 0f;
                break;
            case PATROL:
                combat = Math.round(9f + (float) Math.random() * 3f);
                freighter = Math.round(1f + (float) Math.random() * 3f);
                tanker = Math.round(0f + (float) Math.random() * 1f);
                break;
            case ARMADA:
                combat = Math.round(12f + (float) Math.random() * 8f);
                freighter = Math.round(2f + (float) Math.random() * 4f);
                tanker = Math.round(0f + (float) Math.random() * 2f);
                break;
        }

        CampaignFleetAPI fleet = DS_FleetFactory.enhancedCreateFleet(Global.getSector().getFaction(Factions.PIRATES),
                                                                     (int) (combat + freighter + tanker),
                                                                     new FleetFactoryDelegate() {
                                                                         @Override
                                                                         public CampaignFleetAPI createFleet() {
                                                                             return FleetFactoryV2.createFleet(
                                                                                     new FleetParams(
                                                                                             system.getLocation(), // location
                                                                                             null, // market
                                                                                             Factions.PIRATES,
                                                                                             null, // fleet's faction, if different from above
                                                                                             fleetType,
                                                                                             combat, // combatPts
                                                                                             freighter, // freighterPts
                                                                                             tanker, // tankerPts
                                                                                             0f, // transportPts
                                                                                             0f, // linerPts
                                                                                             0f, // civilianPts
                                                                                             0f, // utilityPts
                                                                                             0f, // qualityBonus
                                                                                             -1f, // qualityOverride
                                                                                             1f, // officer num mult
                                                                                             0 // officer level bonus
                                                                                     ));
                                                                         }
                                                                     });

        if (fleet == null) {
            return null;
        }

        fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_PIRATE, true);

        MarketAPI source = Misc.getSourceMarket(fleet);
        if (source == null) {
            return null;
        }

        if ((float) Math.random() > 0.5f) {
            Vector2f loc = Misc.pickHyperLocationNotNearPlayer(system.getLocation(),
                                                               Global.getSettings().getMaxSensorRange() + 500f);
            Global.getSector().getHyperspace().addEntity(fleet);
            fleet.setLocation(loc.x, loc.y);

            Vector2f dest = Misc.getPointAtRadius(system.getLocation(), 1500);
            LocationAPI hyper = Global.getSector().getHyperspace();
            SectorEntityToken token = hyper.createToken(dest.x, dest.y);
            fleet.addAssignment(FleetAssignment.GO_TO_LOCATION, token, 1000,
                                "travelling to the " + system.getBaseName() + " star system");

            if ((float) Math.random() > 0.75f) {
                fleet.addAssignment(FleetAssignment.RAID_SYSTEM, system.getHyperspaceAnchor(), 20,
                                    "raiding around the " + system.getBaseName() + " star system");
            } else {
                fleet.addAssignment(FleetAssignment.RAID_SYSTEM, system.getCenter(), 20,
                                    "raiding the " + system.getBaseName() + " star system");
            }
        } else {
            Vector2f loc = Misc.pickLocationNotNearPlayer(system,
                                                          Misc.getPointAtRadius(new Vector2f(0, 0), 8000),
                                                          Global.getSettings().getMaxSensorRange() + 500f);
            system.addEntity(fleet);
            fleet.setLocation(loc.x, loc.y);

            fleet.addAssignment(FleetAssignment.RAID_SYSTEM, system.getCenter(), 30,
                                "raiding the " + system.getBaseName() + " star system");
        }

        fleet.addAssignment(FleetAssignment.GO_TO_LOCATION, source.getPrimaryEntity(), 1000,
                            "returning to " + source.getName());
        fleet.addAssignment(FleetAssignment.ORBIT_PASSIVE, source.getPrimaryEntity(), 2f + 2f * (float) Math.random(),
                            "offloading ill-gotten goods");
        fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, source.getPrimaryEntity(), 1000);

        fleet.getMemoryWithoutUpdate().set(KEY_SYSTEM, system.getName());
        return fleet;
    }
}
