package data.scripts.campaign;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.OfficerLevelupPluginImpl;

public class DS_OfficerLevelupPluginImpl extends OfficerLevelupPluginImpl {

    @Override
    public int getMaxLevel(PersonAPI person) {
        if (person != null && person.getFaction() != null && person.getFaction().getId().contentEquals("famous_bounty")) {
            return 29; // yep
        } else if (person != null && person.getMemoryWithoutUpdate().contains("$maxLevel")) {
            return (int) person.getMemoryWithoutUpdate().getFloat("$maxLevel");
        } else {
            return (int) Global.getSettings().getFloat("officerMaxLevel");
        }
    }
}
