package data.scripts.campaign.submarkets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.impl.campaign.ids.ShipRoles;
import com.fs.starfarer.api.impl.campaign.submarkets.MilitarySubmarketPlugin;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.DSModPlugin;
import data.scripts.util.DS_Defs;
import data.scripts.util.DS_Util;
import exerelin.campaign.AllianceManager;
import exerelin.campaign.PlayerFactionStore;

public class DS_MilitarySubmarketPlugin extends MilitarySubmarketPlugin {

    private boolean firstVisit = true;

    public float getPruneAmount() {
        return (float) Math.pow(2f / 3f, Math.max(30f, sinceLastCargoUpdate) / 30f);
    }

    @Override
    public void updateCargoPrePlayerInteraction() {
        if (!DSModPlugin.Module_MarketIntegration) {
            super.updateCargoPrePlayerInteraction();
            return;
        }

        if (!okToUpdateCargo()) {
            return;
        }
        float pruneAmount = getPruneAmount();
        sinceLastCargoUpdate = 0f;

        CargoAPI cargo = getCargo();

        updateEconomicCommoditiesInCargo(false);

        float stability = market.getStabilityValue();

        int iterations = 1;
        if (firstVisit || pruneAmount <= 0.2f) {
            iterations++;
            firstVisit = false;
        }
        for (int i = 0; i < iterations; i++) {
            if (i > 0) {
                sinceLastCargoUpdate = 30f;
                pruneAmount = getPruneAmount();
                sinceLastCargoUpdate = 0f;
            }

            DS_SubmarketUtils.pruneWeapons(pruneAmount, cargo);

            FactionAPI startingFaction = null;
            if (market.getMemoryWithoutUpdate().contains("$startingFactionId")) {
                startingFaction = Global.getSector().getFaction(
                market.getMemoryWithoutUpdate().getString("$startingFactionId"));
            }

            WeightedRandomPicker<FactionAPI> factionPicker = new WeightedRandomPicker<>();
            if (startingFaction != null) {
                factionPicker.add(market.getFaction(), 5f);
                factionPicker.add(startingFaction, 5f);
            } else {
                factionPicker.add(market.getFaction(), 10f);
            }

            if (DS_Defs.MARKET_AFFECTED_FACTIONS.contains(submarket.getFaction().getId())) {
                DS_SubmarketUtils.addWeaponsBasedOnMarketSize(DS_Util.lerp(3.5f, 10f, stability / 10f),
                                                              DS_Util.lerp(1f, 3f, stability / 10f), 3,
                                                              factionPicker, submarket,
                                                              submarket.getMarket().getShipQualityFactor());
            } else {
                addWeaponsBasedOnMarketSize(Math.round(DS_Util.lerp(3.5f, 10f, stability / 10f)),
                                            Math.round(DS_Util.lerp(1f, 3f, stability / 10f)), 3, factionPicker);
            }
            addRandomWeapons(Math.max(1, market.getSize() - 3), 3);
            addRandomWings(Math.max(1, market.getSize() - 4), 3);

            addShips(pruneAmount);
        }
        addHullMods(4, 2 + itemGenRandom.nextInt(4));

        cargo.sort();
        cargo.getMothballedShips().sort();
        DS_SubmarketUtils.finish(cargo, submarket.getFaction());
    }

    private void addShips(float prune) {
        int marketSize = market.getSize();

        pruneShips(prune);

        float mediumShipRarity = 1f;
        float largeShipRarity = 1f;
        float veryLargeShipRarity = 1f;
        float militaryRarity = 1f;
        float countScale = 1f;
        if (DSModPlugin.Module_RareShips) {
            mediumShipRarity = 0.75f;
            largeShipRarity = 0.5f;
            veryLargeShipRarity = 0.33f;
            militaryRarity = 0.67f;
            countScale = 0.5f;
        }

        // 63
        WeightedRandomPicker<String> rolePicker = new WeightedRandomPicker<>();
        rolePicker.add(ShipRoles.FREIGHTER_SMALL, 3f); //5
        rolePicker.add(ShipRoles.TANKER_SMALL, 3f); //5
        rolePicker.add(ShipRoles.PERSONNEL_SMALL, 1f); //5
        rolePicker.add(ShipRoles.COMBAT_SMALL, 15f * militaryRarity); //25
        rolePicker.add(ShipRoles.ESCORT_SMALL, 10f * militaryRarity); //25
        rolePicker.add(ShipRoles.COMBAT_MEDIUM, 15f * mediumShipRarity * militaryRarity); //15
        rolePicker.add(ShipRoles.CARRIER_SMALL, 3f * mediumShipRarity * militaryRarity); //3
		rolePicker.add(ShipRoles.MARKET_RANDOM_SHIPS, 1f); /* Does nothing in DS */

        if (marketSize >= 4) { // 140
            rolePicker.add(ShipRoles.UTILITY, 5f * mediumShipRarity); //5
            rolePicker.add(ShipRoles.FREIGHTER_MEDIUM, 5f * mediumShipRarity); //9
            rolePicker.add(ShipRoles.TANKER_MEDIUM, 5f * mediumShipRarity); //9
            rolePicker.add(ShipRoles.PERSONNEL_MEDIUM, 2f * mediumShipRarity); //9
            rolePicker.add(ShipRoles.COMBAT_MEDIUM, 10f * mediumShipRarity * militaryRarity); //35
            rolePicker.add(ShipRoles.ESCORT_MEDIUM, 10f * mediumShipRarity * militaryRarity); //35
            rolePicker.add(ShipRoles.CARRIER_SMALL, 10f * mediumShipRarity * militaryRarity); //13
            rolePicker.add(ShipRoles.COMBAT_LARGE, 10f * largeShipRarity * militaryRarity); //10
            rolePicker.add(ShipRoles.CARRIER_MEDIUM, 5f * largeShipRarity * militaryRarity); //5
            rolePicker.add(ShipRoles.COMBAT_CAPITAL, 3f * veryLargeShipRarity * militaryRarity); //3
        }

        if (marketSize >= 5) { // 174.5
            rolePicker.add(ShipRoles.UTILITY, 3f * largeShipRarity); //8
            rolePicker.add(ShipRoles.FREIGHTER_LARGE, 3f * largeShipRarity); //5
            rolePicker.add(ShipRoles.TANKER_LARGE, 3f * largeShipRarity); //5
            rolePicker.add(ShipRoles.PERSONNEL_LARGE, 1f * largeShipRarity); //5
            rolePicker.add(ShipRoles.COMBAT_FREIGHTER_LARGE, 5f * largeShipRarity); //5
            rolePicker.add(ShipRoles.COMBAT_LARGE, 5f * largeShipRarity * militaryRarity); //15
            rolePicker.add(ShipRoles.CARRIER_MEDIUM, 5f * largeShipRarity * militaryRarity); //10
            rolePicker.add(ShipRoles.COMBAT_CAPITAL, 4f * veryLargeShipRarity * militaryRarity); //5
            rolePicker.add(ShipRoles.CARRIER_LARGE, 2f * veryLargeShipRarity * militaryRarity); //2
        }

        if (marketSize >= 6) { // 198
            rolePicker.add(ShipRoles.UTILITY, 3f * largeShipRarity); //11
            rolePicker.add(ShipRoles.COMBAT_FREIGHTER_LARGE, 3f * largeShipRarity); //8
            rolePicker.add(ShipRoles.COMBAT_LARGE, 5f * largeShipRarity * militaryRarity); //20
            rolePicker.add(ShipRoles.COMBAT_CAPITAL, 5f * veryLargeShipRarity * militaryRarity); //8
            rolePicker.add(ShipRoles.CARRIER_LARGE, 2f * veryLargeShipRarity * militaryRarity); //4
        }

        float stability = market.getStabilityValue();
        countScale *= DS_Util.lerp(0.5f, 1.1f, stability / 10f);

        // 4/6/8/10/12/14/16/18 [2/3/4/5/6/7/8/9]
        if (DS_Defs.MARKET_AFFECTED_FACTIONS.contains(submarket.getFaction().getId())) {
            DS_SubmarketUtils.addShipsForRoles(Math.round((2 + marketSize * 2) * countScale), rolePicker, null,
                                               submarket, submarket.getMarket().getShipQualityFactor());
        } else {
            addShipsForRoles(Math.round((2 + marketSize * 2) * countScale), rolePicker, null);
        }
    }

    @Override
    protected boolean hasCommission() {
        if (DSModPlugin.isExerelin) {
            String commissionFaction = Misc.getCommissionFactionId();
            if (commissionFaction != null && AllianceManager.areFactionsAllied(commissionFaction,
                                                                               submarket.getFaction().getId())) {
                return true;
            }
            if (AllianceManager.areFactionsAllied(PlayerFactionStore.getPlayerFactionId(),
                                                  submarket.getFaction().getId())) {
                return true;
            }
            return submarket.getFaction().getId().equals(commissionFaction);
        } else {
            return super.hasCommission();
        }
    }
}
