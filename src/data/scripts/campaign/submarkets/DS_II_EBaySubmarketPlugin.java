package data.scripts.campaign.submarkets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.impl.campaign.ids.ShipRoles;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.DSModPlugin;
import data.scripts.util.DS_Defs;
import data.scripts.util.DS_Util;

public class DS_II_EBaySubmarketPlugin extends II_EBaySubmarketPlugin {

    private boolean firstVisit = true;

    public float getPruneAmount() {
        return (float) Math.pow(0.5f, Math.max(30f, sinceLastCargoUpdate) / 30f);
    }

    @Override
    public void updateCargoPrePlayerInteraction() {
        if (!DSModPlugin.Module_MarketIntegration) {
            super.updateCargoPrePlayerInteraction();
            return;
        }

        if (!okToUpdateCargo()) {
            return;
        }
        float pruneAmount = getPruneAmount();
        sinceLastCargoUpdate = 0f;

        CargoAPI cargo = getCargo();
        float stability = market.getStabilityValue();

        WeightedRandomPicker<FactionAPI> factionPicker = new WeightedRandomPicker<>();
        factionPicker.add(submarket.getFaction(), 10f);
        for (FactionAPI faction : Global.getSector().getAllFactions()) {
            if (!faction.isShowInIntelTab()) {
                continue;
            }
            if (faction.isHostileTo(submarket.getFaction())) {
                continue;
            }
            float weight;
            if (faction.isAtWorst(submarket.getFaction(), RepLevel.COOPERATIVE)) {
                weight = 10f;
            } else if (faction.isAtWorst(submarket.getFaction(), RepLevel.FRIENDLY)) {
                weight = 7f;
            } else if (faction.isAtWorst(submarket.getFaction(), RepLevel.WELCOMING)) {
                weight = 5f;
            } else if (faction.isAtWorst(submarket.getFaction(), RepLevel.FAVORABLE)) {
                weight = 4f;
            } else if (faction.isAtWorst(submarket.getFaction(), RepLevel.NEUTRAL)) {
                weight = 3f;
            } else if (faction.isAtWorst(submarket.getFaction(), RepLevel.SUSPICIOUS)) {
                weight = 2f;
            } else {
                weight = 1f;
            }
            factionPicker.add(faction, weight);
        }

        int iterations = 1;
        if (firstVisit || pruneAmount <= 0.2f) {
            iterations++;
            firstVisit = false;
        }
        for (int i = 0; i < iterations; i++) {
            if (i > 0) {
                sinceLastCargoUpdate = 30f;
                pruneAmount = getPruneAmount();
                sinceLastCargoUpdate = 0f;
            }

            DS_SubmarketUtils.pruneWeapons(pruneAmount, cargo);
            if (DS_Defs.MARKET_AFFECTED_FACTIONS.contains(submarket.getFaction().getId())) {
                DS_SubmarketUtils.addWeaponsBasedOnMarketSize(DS_Util.lerp(6f, 15f, stability / 10f),
                                                              DS_Util.lerp(3f, 5f, stability / 10f), 3,
                                                              factionPicker, submarket,
                                                              submarket.getMarket().getShipQualityFactor());
            } else {
                addWeaponsBasedOnMarketSize(Math.round(DS_Util.lerp(6f, 15f, stability / 10f)),
                                            Math.round(DS_Util.lerp(3f, 5f, stability / 10f)), 3, factionPicker);
            }
            addRandomWeapons(Math.max(1, market.getSize() - 2), 3);
            addRandomWings(Math.max(1, market.getSize() - 3), 3);

            addShips(pruneAmount, factionPicker);
        }
        addHullMods(4, 3 + itemGenRandom.nextInt(6));

        cargo.sort();
        cargo.getMothballedShips().sort();
        DS_SubmarketUtils.finish(cargo, submarket.getFaction());
    }

    private void addShips(float prune, WeightedRandomPicker<FactionAPI> factionPicker) {
        int marketSize = market.getSize();

        pruneShips(prune);

        float mediumShipRarity = 1f;
        float largeShipRarity = 1f;
        float veryLargeShipRarity = 1f;
        float militaryRarity = 1f;
        float countScale = 1f;
        if (DSModPlugin.Module_RareShips) {
            mediumShipRarity = 0.75f;
            largeShipRarity = 0.5f;
            veryLargeShipRarity = 0.33f;
            militaryRarity = 0.67f;
            countScale = 0.5f;
        }

        // 80
        WeightedRandomPicker<String> rolePicker = new WeightedRandomPicker<>();
        rolePicker.add(ShipRoles.FREIGHTER_SMALL, 3f); //7
        rolePicker.add(ShipRoles.TANKER_SMALL, 3f); //7
        rolePicker.add(ShipRoles.PERSONNEL_SMALL, 2f); //7
        rolePicker.add(ShipRoles.LINER_SMALL, 1f); //7
        rolePicker.add(ShipRoles.COMBAT_FREIGHTER_SMALL, 15f); //15
        rolePicker.add(ShipRoles.COMBAT_SMALL, 15f * militaryRarity); //25
        rolePicker.add(ShipRoles.ESCORT_SMALL, 10f * militaryRarity); //25
        rolePicker.add(ShipRoles.COMBAT_MEDIUM, 15f * mediumShipRarity * militaryRarity); //15
        rolePicker.add(ShipRoles.CARRIER_SMALL, 3f * mediumShipRarity * militaryRarity); //3

        if (marketSize >= 4) { // 170
            rolePicker.add(ShipRoles.UTILITY, 5f * mediumShipRarity); //5
            rolePicker.add(ShipRoles.FREIGHTER_MEDIUM, 5f * mediumShipRarity); //12
            rolePicker.add(ShipRoles.TANKER_MEDIUM, 5f * mediumShipRarity); //12
            rolePicker.add(ShipRoles.PERSONNEL_MEDIUM, 3f * mediumShipRarity); //12
            rolePicker.add(ShipRoles.LINER_MEDIUM, 2f * mediumShipRarity); //12
            rolePicker.add(ShipRoles.COMBAT_FREIGHTER_MEDIUM, 10f * mediumShipRarity); //10
            rolePicker.add(ShipRoles.COMBAT_MEDIUM, 10f * mediumShipRarity * militaryRarity); //35
            rolePicker.add(ShipRoles.ESCORT_MEDIUM, 10f * mediumShipRarity * militaryRarity); //35
            rolePicker.add(ShipRoles.CARRIER_SMALL, 10f * mediumShipRarity * militaryRarity); //13
            rolePicker.add(ShipRoles.COMBAT_LARGE, 10f * largeShipRarity * militaryRarity); //10
            rolePicker.add(ShipRoles.CARRIER_MEDIUM, 5f * largeShipRarity * militaryRarity); //5
            rolePicker.add(ShipRoles.COMBAT_CAPITAL, 3f * veryLargeShipRarity * militaryRarity); //3
        }

        if (marketSize >= 5) { // 221.5
            rolePicker.add(ShipRoles.UTILITY, 3f * largeShipRarity); //8
            rolePicker.add(ShipRoles.FREIGHTER_LARGE, 3f * largeShipRarity); //7
            rolePicker.add(ShipRoles.TANKER_LARGE, 3f * largeShipRarity); //7
            rolePicker.add(ShipRoles.PERSONNEL_LARGE, 2f * largeShipRarity); //7
            rolePicker.add(ShipRoles.LINER_LARGE, 1f * largeShipRarity); //7
            rolePicker.add(ShipRoles.COMBAT_FREIGHTER_LARGE, 10f * largeShipRarity); //10
            rolePicker.add(ShipRoles.COMBAT_LARGE, 5f * largeShipRarity * militaryRarity); //15
            rolePicker.add(ShipRoles.CARRIER_MEDIUM, 5f * largeShipRarity * militaryRarity); //10
            rolePicker.add(ShipRoles.COMBAT_CAPITAL, 4f * veryLargeShipRarity * militaryRarity); //5
            rolePicker.add(ShipRoles.CARRIER_LARGE, 2f * veryLargeShipRarity * militaryRarity); //2
        }

        if (marketSize >= 6) { // 233
            rolePicker.add(ShipRoles.UTILITY, 3f * largeShipRarity); //11
            rolePicker.add(ShipRoles.COMBAT_FREIGHTER_LARGE, 3f * largeShipRarity); //13
            rolePicker.add(ShipRoles.COMBAT_LARGE, 5f * largeShipRarity * militaryRarity); //20
            rolePicker.add(ShipRoles.COMBAT_CAPITAL, 5f * veryLargeShipRarity * militaryRarity); //8
            rolePicker.add(ShipRoles.CARRIER_LARGE, 2f * veryLargeShipRarity * militaryRarity); //4
        }

        float stability = market.getStabilityValue();
        countScale *= DS_Util.lerp(0.5f, 1.1f, stability / 10f);

        // 4/6/8/10/12/14/16/18 [2/3/4/5/6/7/8/9]
        if (DS_Defs.MARKET_AFFECTED_FACTIONS.contains(submarket.getFaction().getId())) {
            DS_SubmarketUtils.addShipsForRoles(Math.round((4 + marketSize * 2) * countScale), rolePicker, factionPicker,
                                               submarket, submarket.getMarket().getShipQualityFactor());
        } else {
            addShipsForRoles(Math.round((4 + marketSize * 2) * countScale), rolePicker, factionPicker);
        }
    }
}
