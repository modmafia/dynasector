package data.scripts.campaign.submarkets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.ShipRoles;
import com.fs.starfarer.api.impl.campaign.submarkets.OpenMarketPlugin;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.DSModPlugin;
import data.scripts.util.DS_Defs;
import data.scripts.util.DS_Util;

public class DS_OpenMarketPlugin extends OpenMarketPlugin {

    private boolean firstVisit = true;

    public float getPruneAmount() {
        return (float) Math.pow(2f / 3f, Math.max(30f, sinceLastCargoUpdate) / 30f);
    }

    @Override
    public void updateCargoPrePlayerInteraction() {
        if (!DSModPlugin.Module_MarketIntegration) {
            super.updateCargoPrePlayerInteraction();
            return;
        }

        if (!okToUpdateCargo()) {
            return;
        }
        float pruneAmount = getPruneAmount();
        sinceLastCargoUpdate = 0f;

        CargoAPI cargo = getCargo();

        updateEconomicCommoditiesInCargo(false);

        float stability = market.getStabilityValue();

        int iterations = 1;
        if (firstVisit || pruneAmount <= 0.2f) {
            iterations++;
            firstVisit = false;
        }
        for (int i = 0; i < iterations; i++) {
            if (i > 0) {
                sinceLastCargoUpdate = 30f;
                pruneAmount = getPruneAmount();
                sinceLastCargoUpdate = 0f;
            }

            DS_SubmarketUtils.pruneWeapons(pruneAmount, cargo);

            FactionAPI startingFaction = null;
            if (market.getMemoryWithoutUpdate().contains("$startingFactionId")) {
                startingFaction = Global.getSector().getFaction(
                market.getMemoryWithoutUpdate().getString("$startingFactionId"));
            }

            WeightedRandomPicker<FactionAPI> factionPicker = new WeightedRandomPicker<>();
            if (startingFaction != null) {
                factionPicker.add(market.getFaction(), 3.75f);
                factionPicker.add(startingFaction, 3.75f);
            } else {
                factionPicker.add(market.getFaction(), 7.5f);
            }
            factionPicker.add(Global.getSector().getFaction(Factions.INDEPENDENT), 2.5f);

            if (DS_Defs.MARKET_AFFECTED_FACTIONS.contains(submarket.getFaction().getId())) {
                DS_SubmarketUtils.addWeaponsBasedOnMarketSize(DS_Util.lerp(1.5f, 5f, stability / 10f),
                                                              DS_Util.lerp(0.5f, 1f, stability / 10f), 0,
                                                              factionPicker, submarket,
                                                              submarket.getMarket().getShipQualityFactor());
            } else {
                addWeaponsBasedOnMarketSize(Math.round(DS_Util.lerp(1.5f, 5f, stability / 10f)),
                                            Math.round(DS_Util.lerp(0.5f, 1f, stability / 10f)), 0, factionPicker);
            }
            addRandomWeapons(Math.max(1, market.getSize() - 3), 0);
            addRandomWings(Math.max(1, market.getSize() - 5), 0);

            addShips(pruneAmount);
        }
        addHullMods(1, 1 + itemGenRandom.nextInt(3));

        cargo.sort();
        cargo.getMothballedShips().sort();
        DS_SubmarketUtils.finish(cargo, submarket.getFaction());
    }

    private void addShips(float prune) {
        int marketSize = market.getSize();

        pruneShips(prune);

        float mediumShipRarity = 1f;
        float largeShipRarity = 1f;
        float countScale = 1f;
        if (DSModPlugin.Module_RareShips) {
            mediumShipRarity = 0.75f;
            largeShipRarity = 0.5f;
            countScale = 0.5f;
        }

        // 52
        WeightedRandomPicker<String> rolePicker = new WeightedRandomPicker<>();
        rolePicker.add(ShipRoles.CRIG, 10f); //20
        rolePicker.add(ShipRoles.TUG, 10f); //20
        rolePicker.add(ShipRoles.CIV_RANDOM, 10f); //26
        rolePicker.add(ShipRoles.FREIGHTER_SMALL, 10f); //26
        rolePicker.add(ShipRoles.TANKER_SMALL, 20f); //26
        rolePicker.add(ShipRoles.PERSONNEL_SMALL, 3f); //26
        rolePicker.add(ShipRoles.COMBAT_FREIGHTER_SMALL, 5f); //6
        rolePicker.add(ShipRoles.LINER_SMALL, 1f); //1

        if (marketSize >= 4) { // 67.5
            rolePicker.add(ShipRoles.UTILITY, 5f * mediumShipRarity); //25
            rolePicker.add(ShipRoles.FREIGHTER_MEDIUM, 5f * mediumShipRarity); //7
            rolePicker.add(ShipRoles.TANKER_MEDIUM, 5f * mediumShipRarity); //7
            rolePicker.add(ShipRoles.PERSONNEL_MEDIUM, 1f * mediumShipRarity); //7
            rolePicker.add(ShipRoles.COMBAT_FREIGHTER_MEDIUM, 3f * mediumShipRarity); //3
            rolePicker.add(ShipRoles.LINER_MEDIUM, 0.5f * mediumShipRarity); //0.5
        }

        if (marketSize >= 6) { // 77.75
            rolePicker.add(ShipRoles.UTILITY, 3f * largeShipRarity); //28
            rolePicker.add(ShipRoles.FREIGHTER_LARGE, 3f * largeShipRarity); //5
            rolePicker.add(ShipRoles.TANKER_LARGE, 3f * largeShipRarity); //5
            rolePicker.add(ShipRoles.PERSONNEL_LARGE, 1f * largeShipRarity); //5
            rolePicker.add(ShipRoles.COMBAT_FREIGHTER_LARGE, 2f * largeShipRarity); //2
            rolePicker.add(ShipRoles.LINER_LARGE, 1f * largeShipRarity); //0.25
        }

        float stability = market.getStabilityValue();
        countScale *= DS_Util.lerp(0.5f, 1.25f, stability / 10f);

        // 3/4/5/6/7/8/9/10 [1/2/2/3/3/4/4/5]
        if (DS_Defs.MARKET_AFFECTED_FACTIONS.contains(submarket.getFaction().getId())) {
            DS_SubmarketUtils.addShipsForRoles(Math.round((2 + marketSize) * countScale), rolePicker, null, submarket,
                                               submarket.getMarket().getShipQualityFactor());
        } else {
            addShipsForRoles(Math.round((2 + marketSize) * countScale), rolePicker, null);
        }
    }
}
