package data.scripts.campaign.submarkets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.ShipRoles;
import com.fs.starfarer.api.impl.campaign.submarkets.BlackMarketPlugin;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.DSModPlugin;
import data.scripts.util.DS_Defs;
import data.scripts.util.DS_Util;

public class DS_BlackMarketPlugin extends BlackMarketPlugin {

    private boolean firstVisit = true;

    public float getPruneAmount() {
        return (float) Math.pow(0.5f, Math.max(30f, sinceLastCargoUpdate) / 30f);
    }

    @Override
    public void updateCargoPrePlayerInteraction() {
        if (!DSModPlugin.Module_MarketIntegration) {
            super.updateCargoPrePlayerInteraction();
            return;
        }

        if (!okToUpdateCargo()) {
            return;
        }
        float pruneAmount = getPruneAmount();
        sinceLastCargoUpdate = 0f;

        CargoAPI cargo = getCargo();

        updateEconomicCommoditiesInCargo(true);

        float stability = market.getStabilityValue();

        int iterations = 1;
        if (firstVisit || pruneAmount <= 0.2f) {
            iterations++;
            firstVisit = false;
        }
        for (int i = 0; i < iterations; i++) {
            if (i > 0) {
                sinceLastCargoUpdate = 30f;
                pruneAmount = getPruneAmount();
                sinceLastCargoUpdate = 0f;
            }

            DS_SubmarketUtils.pruneWeapons(pruneAmount, cargo);

            FactionAPI startingFaction = null;
            if (market.getMemoryWithoutUpdate().contains("$startingFactionId")) {
                startingFaction = Global.getSector().getFaction(
                market.getMemoryWithoutUpdate().getString("$startingFactionId"));
            }

            WeightedRandomPicker<FactionAPI> factionPicker = new WeightedRandomPicker<>();
            if (startingFaction != null) {
                factionPicker.add(market.getFaction(), (11f - stability) / 2f);
                factionPicker.add(startingFaction, (11f - stability) / 2f);
            } else {
                factionPicker.add(market.getFaction(), 11f - stability);
            }
            if (!submarket.getFaction().getId().contentEquals(Factions.PIRATES)) {
                factionPicker.add(Global.getSector().getFaction(Factions.INDEPENDENT), 4f);
            }
            factionPicker.add(submarket.getFaction(), 6f);

            if (DS_Defs.MARKET_AFFECTED_FACTIONS.contains(submarket.getFaction().getId())) {
                DS_SubmarketUtils.addBlackMarketWeapons(DS_Util.lerp(5f, 1.5f, stability / 10f),
                                                        DS_Util.lerp(1f, 0.5f, stability / 10f), 1, factionPicker,
                                                        submarket, submarket.getMarket().getShipQualityFactor());
            } else {
                addWeaponsBasedOnMarketSize(Math.round(DS_Util.lerp(5f, 1.5f, stability / 10f)),
                                            Math.round(DS_Util.lerp(1f, 0.5f, stability / 10f)), 1, factionPicker);
            }

            addRandomWeapons(Math.max(1, market.getSize() - 3), 2);
            addRandomWings(Math.max(1, market.getSize() - 5), 2);

            addShips(pruneAmount);
        }
        addHullMods(1, 1 + itemGenRandom.nextInt(3));

        cargo.sort();
        cargo.getMothballedShips().sort();
        DS_SubmarketUtils.finish(cargo, submarket.getFaction());
    }

    private void addShips(float prune) {
        int marketSize = market.getSize();

        pruneShips(prune);

        float mediumShipRarity = 1f;
        float largeShipRarity = 1f;
        float militaryRarity = 1f;
        float countScale = 1f;
        if (DSModPlugin.Module_RareShips) {
            mediumShipRarity = 0.75f;
            largeShipRarity = 0.5f;
            militaryRarity = 0.67f;
            countScale = 0.5f;
        }

        // 71
        WeightedRandomPicker<String> rolePicker = new WeightedRandomPicker<>();
        rolePicker.add(ShipRoles.CIV_RANDOM, 5f); //10
        rolePicker.add(ShipRoles.FREIGHTER_SMALL, 3f); //10
        rolePicker.add(ShipRoles.TANKER_SMALL, 3f); //10
        rolePicker.add(ShipRoles.PERSONNEL_SMALL, 1f); //10
        rolePicker.add(ShipRoles.COMBAT_FREIGHTER_SMALL, 15f); //15
        rolePicker.add(ShipRoles.COMBAT_SMALL, 15f * militaryRarity); //15
        rolePicker.add(ShipRoles.COMBAT_MEDIUM, 15f * militaryRarity * mediumShipRarity); //15
        rolePicker.add(ShipRoles.LINER_SMALL, 1f); //1
		rolePicker.add(ShipRoles.MARKET_RANDOM_SHIPS, 1f); /* Does nothing in DS */

        if (marketSize >= 4) { // 129
            rolePicker.add(ShipRoles.UTILITY, 5f * mediumShipRarity); //5
            rolePicker.add(ShipRoles.FREIGHTER_MEDIUM, 5f * mediumShipRarity); //7
            rolePicker.add(ShipRoles.TANKER_MEDIUM, 5f * mediumShipRarity); //7
            rolePicker.add(ShipRoles.PERSONNEL_MEDIUM, 1f * mediumShipRarity); //7
            rolePicker.add(ShipRoles.COMBAT_FREIGHTER_MEDIUM, 10f * mediumShipRarity); //10
            rolePicker.add(ShipRoles.COMBAT_MEDIUM, 10f * militaryRarity * mediumShipRarity); //10
            rolePicker.add(ShipRoles.CARRIER_SMALL, 10f * militaryRarity * mediumShipRarity); //10
            rolePicker.add(ShipRoles.LINER_MEDIUM, 1f); //1
        }

        if (marketSize >= 6) { // 173
            rolePicker.add(ShipRoles.UTILITY, 3f * largeShipRarity); //9
            rolePicker.add(ShipRoles.FREIGHTER_LARGE, 3f * largeShipRarity); //5
            rolePicker.add(ShipRoles.TANKER_LARGE, 3f * largeShipRarity); //5
            rolePicker.add(ShipRoles.PERSONNEL_LARGE, 1f * largeShipRarity); //5
            rolePicker.add(ShipRoles.COMBAT_FREIGHTER_LARGE, 5f * largeShipRarity); //5
            rolePicker.add(ShipRoles.COMBAT_LARGE, 5f * militaryRarity * largeShipRarity); //5
            rolePicker.add(ShipRoles.CARRIER_MEDIUM, 10f * militaryRarity * largeShipRarity); //10
            rolePicker.add(ShipRoles.LINER_LARGE, 1f); //1
        }

        WeightedRandomPicker<FactionAPI> factionPicker = new WeightedRandomPicker<>();
        float stability = market.getStabilityValue();
        factionPicker.add(market.getFaction(), 11f - stability);
        if (!submarket.getFaction().getId().contentEquals(Factions.PIRATES)) {
            factionPicker.add(Global.getSector().getFaction(Factions.INDEPENDENT), 4f);
        }
        factionPicker.add(submarket.getFaction(), 6f);
        countScale *= DS_Util.lerp(1.25f, 0.75f, stability / 10f);

        // 2/4/6/8/10/12/14/16 [1/2/3/4/5/6/7/8]
        if (DS_Defs.MARKET_AFFECTED_FACTIONS.contains(submarket.getFaction().getId())) {
            DS_SubmarketUtils.addShipsForRoles(Math.round(Math.max(1, (marketSize * 2) * countScale)), rolePicker,
                                               factionPicker, submarket, submarket.getMarket().getShipQualityFactor());
        } else {
            addShipsForRoles(Math.round(Math.max(1, (marketSize * 2) * countScale)), rolePicker, factionPicker);
        }
    }
}
