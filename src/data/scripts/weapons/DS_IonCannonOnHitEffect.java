package data.scripts.weapons;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnHitEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import data.scripts.DSModPlugin;
import data.scripts.hullmods.TEM_LatticeShield;
import java.awt.Color;
import org.lwjgl.util.vector.Vector2f;

public class DS_IonCannonOnHitEffect implements OnHitEffectPlugin {

    @Override
    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit,
            CombatEngineAPI engine) {
        boolean hitShields = shieldHit;

        if (target instanceof ShipAPI) {
            ShipAPI ship = (ShipAPI) target;
            if (!hitShields) {
                if (DSModPlugin.templarsExists && ship.getVariant().getHullMods().contains("tem_latticeshield")
                        && TEM_LatticeShield.shieldLevel(ship) > 0f) {
                    hitShields = true;
                }
            }

            if ((float) Math.random() > 0.75f && !hitShields) {
                float emp = projectile.getEmpAmount();
                float dam = projectile.getDamageAmount();

                ShipAPI empTarget = ship;
                engine.spawnEmpArc(projectile.getSource(), point, empTarget, empTarget, DamageType.ENERGY, dam, emp,
                        100000f, "tachyon_lance_emp_impact", 20f,
                        new Color(25, 100, 155, 255), new Color(255, 255, 255, 255));
            }
        }
    }
}
