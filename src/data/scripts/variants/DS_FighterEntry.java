package data.scripts.variants;

import java.util.LinkedHashMap;
import java.util.Map;

public class DS_FighterEntry {

    public final int OP;
    public final boolean alpha;
    public final boolean attack;
    public final boolean builtIn;
    public final boolean closeRange;
    public final boolean defense;
    public final boolean disable;
    public final boolean energy;
    public final Map<String, Float> factionWeights = new LinkedHashMap<>(30);
    public final boolean fragmentation;
    public final boolean highExplosive;
    public final boolean interfere;
    public final boolean kinetic;
    public final float leetness;
    public final boolean longRange;
    public final boolean support;
    public final String techType;
    public final int tier;

    public DS_FighterEntry(boolean builtIn, int tier, int OP, boolean kinetic, boolean highExplosive, boolean energy,
                           boolean fragmentation, boolean attack, boolean interfere, boolean alpha, boolean defense,
                           boolean support, boolean closeRange, boolean longRange, boolean disable, String techType,
                           float leetness) {
        this.builtIn = builtIn;
        this.tier = tier;
        this.OP = OP;
        this.kinetic = kinetic;
        this.highExplosive = highExplosive;
        this.energy = energy;
        this.fragmentation = fragmentation;
        this.attack = attack;
        this.interfere = interfere;
        this.alpha = alpha;
        this.defense = defense;
        this.support = support;
        this.closeRange = closeRange;
        this.longRange = longRange;
        this.disable = disable;
        this.techType = techType;
        this.leetness = leetness;
    }
}
