package data.scripts.variants;

import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponSize;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponType;
import java.util.LinkedHashMap;
import java.util.Map;

public class DS_WeaponEntry {

    public final int OP;
    public final boolean alpha;
    public final boolean attack;
    public final boolean builtIn;
    public final boolean closeRange;
    public final DamageType damageType;
    public final boolean defense;
    public final boolean disable;
    public final Map<String, Float> factionWeights = new LinkedHashMap<>(30);
    public final boolean frontOnly;
    public final float leetness;
    public final boolean limitedAmmo;
    public final boolean longRange;
    public final boolean lowFlux;
    public final boolean separateFire;
    public final WeaponSize size;
    public final boolean standoff;
    public final String techType;
    public final int tier;
    public final boolean turretOnly;
    public final WeaponType type;

    public DS_WeaponEntry(boolean builtIn, WeaponSize size, WeaponType type, int tier, int OP, boolean limitedAmmo,
                          boolean lowFlux, boolean separateFire, boolean frontOnly, boolean turretOnly,
                          DamageType damageType, boolean attack, boolean standoff, boolean alpha, boolean defense,
                          boolean closeRange, boolean longRange, boolean disable, String techType, float leetness) {
        this.builtIn = builtIn;
        this.size = size;
        this.type = type;
        this.tier = tier;
        this.OP = OP;
        this.limitedAmmo = limitedAmmo;
        this.lowFlux = lowFlux;
        this.separateFire = separateFire;
        this.frontOnly = frontOnly;
        this.turretOnly = turretOnly;
        this.damageType = damageType;
        this.attack = attack;
        this.standoff = standoff;
        this.alpha = alpha;
        this.defense = defense;
        this.closeRange = closeRange;
        this.longRange = longRange;
        this.disable = disable;
        this.techType = techType;
        this.leetness = leetness;
    }
}
