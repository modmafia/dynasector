package data.console.commands;

import com.fs.starfarer.api.Global;
import data.scripts.variants.DS_RandomizerTester;
import org.lazywizard.console.BaseCommand;
import org.lazywizard.console.CommonStrings;
import org.lazywizard.console.Console;

public class EnableRandomizerTester implements BaseCommand {

    @Override
    public CommandResult runCommand(String args, CommandContext context) {
        if (context != CommandContext.CAMPAIGN_MAP) {
            Console.showMessage(CommonStrings.ERROR_CAMPAIGN_ONLY);
            return CommandResult.WRONG_CONTEXT;
        }

        if (!Global.getSector().hasScript(DS_RandomizerTester.class)) {
            Global.getSector().addScript(new DS_RandomizerTester());
            Console.showMessage("Tester enabled!");
            return CommandResult.SUCCESS;
        } else {
            Global.getSector().removeScriptsOfClass(DS_RandomizerTester.class);
            Console.showMessage("Tester disabled!");
            return CommandResult.SUCCESS;
        }
    }
}
