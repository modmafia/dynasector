package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.util.Misc.Token;
import java.util.List;
import java.util.Map;

/**
 * DS_NGCSetShipType
 */
public class DS_NGCSetShipType extends BaseCommandPlugin {

    @Override
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Token> params,
                           Map<String, MemoryAPI> memoryMap) {
        String param = params.get(0).getString(memoryMap);
        String type = param.substring("ngcShipType".length());
        memoryMap.get(MemKeys.LOCAL).set("$ngcShipType", type, 7);

        return true;
    }
}
