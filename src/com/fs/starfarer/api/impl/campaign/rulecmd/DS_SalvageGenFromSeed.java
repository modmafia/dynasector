package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.ids.FleetTypes;
import com.fs.starfarer.api.impl.campaign.ids.MemFlags;
import com.fs.starfarer.api.impl.campaign.procgen.DefenderDataOverride;
import com.fs.starfarer.api.impl.campaign.procgen.SalvageEntityGenDataSpec;
import com.fs.starfarer.api.impl.campaign.procgen.themes.SalvageEntityGeneratorOld;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.SalvageGenFromSeed;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.Misc.Token;
import data.scripts.campaign.fleets.DS_FleetInjector;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * DS_SalvageGenFromSeed
 */
public class DS_SalvageGenFromSeed extends SalvageGenFromSeed {

    @Override
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Token> params,
                           final Map<String, MemoryAPI> memoryMap) {
        if (dialog == null) {
            return false;
        }

        SectorEntityToken entity = dialog.getInteractionTarget();
        SalvageEntityGenDataSpec spec = SalvageEntityGeneratorOld.getSalvageSpec(entity.getCustomEntityType());

        MemoryAPI memory = memoryMap.get(MemKeys.LOCAL);
        if (memoryMap.containsKey(MemKeys.ENTITY)) {
            memory = memoryMap.get(MemKeys.ENTITY);
        }

        long seed = memory.getLong(MemFlags.SALVAGE_SEED);
//		if (Global.getSettings().isDevMode()) {
//			seed = new Random().nextLong();
//		}

        // don't use seed directly so that results from different uses of it aren't tied together
        Random random = Misc.getRandom(seed, 0);

        DefenderDataOverride override = null;
        if (memory.contains(MemFlags.SALVAGE_DEFENDER_OVERRIDE)) {
            override = (DefenderDataOverride) memory.get(MemFlags.SALVAGE_DEFENDER_OVERRIDE);
        }

        Random fleetRandom = Misc.getRandom(seed, 1);
        float strength = spec.getMinStr() + Math.round((spec.getMaxStr() - spec.getMinStr()) * fleetRandom.nextFloat());
        float prob = spec.getProbDefenders();

        if (override != null) {
            strength = override.minStr + Math.round((override.maxStr - override.minStr) * fleetRandom.nextFloat());
            prob = override.probDefenders;
        }

        String factionId = entity.getFaction().getId();
        if (spec.getDefFaction() != null) {
            factionId = spec.getDefFaction();
        }
        if (override != null && override.defFaction != null) {
            factionId = override.defFaction;
        }

        SDMParams p = new SDMParams();
        p.entity = entity;
        p.factionId = factionId;

        SalvageDefenderModificationPlugin plugin = Global.getSector().getGenericPlugins().pickPlugin(
                                          SalvageDefenderModificationPlugin.class, p);

        if (plugin != null) {
            strength = plugin.getStrength(p, strength, random, override != null);
            prob = plugin.getProbability(p, prob, random, override != null);
        }

        if ((int) strength > 0 && //false &&
                random.nextFloat() < prob &&
                !memory.getBoolean("$defenderFleetDefeated")) {
            memory.set("$hasDefenders", true, 0);

            if (!memory.contains("$defenderFleet")) {
                MarketAPI market = Global.getFactory().createMarket("temp", "Temp", 1);

                market.setFactionId(factionId);

                FleetParams fleetParams = new FleetParams(
                            null,
                            market,
                            market.getFactionId(),
                            null, // fleet's faction, if different from above, which is also used for source market picking
                            FleetTypes.PATROL_SMALL,
                            (int) strength, // combatPts
                            0, // freighterPts
                            0, // tankerPts
                            0f, // transportPts
                            0f, // linerPts
                            0f, // civilianPts
                            0f, // utilityPts
                            0f, // qualityBonus
                            1f, // qualityOverride
                            0f, // officer num mult
                            0 // officer level bonus
                    );
                fleetParams.random = fleetRandom;
                fleetParams.withOfficers = false;
                fleetParams.maxShipSize = (int) spec.getMaxDefenderSize();
                if (override != null) {
                    fleetParams.maxShipSize = override.maxDefenderSize;
                }

                if (plugin != null) {
                    fleetParams.maxShipSize = (int) (plugin.getMaxSize(p, fleetParams.maxShipSize, random, override !=
                                                                       null));
                }

                CampaignFleetAPI defenders = FleetFactoryV2.createFleet(fleetParams);
                //defenders.setName(entity.getName() + ": " + "Automated Defenses");
                defenders.setName("Automated Defenses");

                float probStation = spec.getProbStation();
                if (override != null) {
                    probStation = override.probStation;
                }
                if (fleetRandom.nextFloat() < probStation) {
                    String stationRole = spec.getStationRole();
                    if (override != null && override.stationRole != null) {
                        stationRole = override.stationRole;
                    }
                    market.getFaction().pickShipAndAddToFleet(stationRole, 1f, defenders, fleetRandom);
                    defenders.getFleetData().sort();
                }

                defenders.clearAbilities();

                if (plugin != null) {
                    plugin.modifyFleet(p, defenders, fleetRandom, override != null);
                }

                defenders.getFleetData().sort();

                DS_FleetInjector.prepFleet(defenders);
                DS_FleetInjector.injectFleet(defenders);
                defenders.getMemoryWithoutUpdate().set("$dynasectorInjected", true);

                memory.set("$defenderFleet", defenders, 0);
            }

            CampaignFleetAPI defenders = memory.getFleet("$defenderFleet");
            if (defenders != null) {
                boolean hasStation = false;
                boolean hasNonStation = false;
                for (FleetMemberAPI member : defenders.getFleetData().getMembersListCopy()) {
                    if (member.isStation()) {
                        hasStation = true;
                    } else {
                        hasNonStation = true;
                    }
                }
                memory.set("$hasStation", hasStation, 0);
                memory.set("$hasNonStation", hasNonStation, 0);

                defenders.setLocation(entity.getLocation().x, entity.getLocation().y);
            }
        } else {
            memory.set("$hasDefenders", false, 0);
            memory.set("$hasStation", false, 0);
        }

        //memory.set("$hasSalvageSpecial", false, 0);
        //memory.set("$salvageSpecialData", null, 0);
        //memory.set("$salvageSpecialData", new DomainSurveyDerelictSpecialData(SpecialType.SCRAMBLED), 0);
        return true;
    }
}
